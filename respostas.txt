/***** MAC0110 - EP3 *****/
  Nome: Willian Wang
  NUSP: 11735380

/***** Parte 1 - Entendendo o código *****/

    1) Qual a probabilidade de um elemento da matriz ser grama (ou terreno)? Você pode expressar essa possibilidade utilizando um valor numérico ou uma fórmula.

        0.65

    2) Analise o código e diga: qual a diferença entre o terreno e o terreno especial?

        Nenhuma, apenas a probabilidade de cada um aparecer (o especial é mais raro)

    3) Dados os valores iniciais das constantes, qual a energia necessária para um lobo se reproduzir? E um coelho?

        20 e 12 respectivamente

/***** Parte 2 - Completando as funções *****/

    4) Ao contrário das funções ocupa_vizinho! e reproduz!, a função morre! não recebe a matriz de energia como parâmetro. Por quê não é necessário alterar a energia com a morte de um animal?

        Porque a função morre! é aplicada somente em animais com energia = 0, ou seja, já é a mesma energia que uma grama teria se estivesse no seu lugar 

/***** Parte 3 - Teste e simulação *****/

    5) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece na ilha?

        Não sobra nenhum coelho, a quantidade de lobos fica em torno de 15, a energia em torno de 400 e a comida por volta de 50

    6) Qual combinação de constantes leva a ilha a ser dominada por coelhos? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        PROBABILIDADE_LOBO = 0.0; PROBABILIDADE_COELHO = 0.5; PROBABILIDADE_COMIDA = 0.4; REGENERACAO_TERRENO = 0.9; FATOR_REPRODUCAO = 2; TAMANHO_ILHA = 20

    7) Qual combinação de constantes leva a ilha não ter nenhum animal? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        PROBABILIDADE_LOBO = 0.0; PROBABILIDADE_COELHO = 0.0; PROBABILIDADE_COMIDA = 0.9; REGENERACAO_TERRENO = 0.9; FATOR_REPRODUCAO = 2; TAMANHO_ILHA = 20

/***** Parte 3 - Usando DataFrames e plotando gráficos *****/

    8) Qual a diferença entre a função simula - da parte 3 - e a função simula2?

        A função simula2 faz a mesma simulação que a primeira função mencionada, porém ela também registra o estado de cada instante da simulação e guarda numa tabela

    9) A função gera_graficos possui sintaxes diferentes das vistas em aula e nos outros exercícios. Apesar disso, é possível entender o que a função faz, sem rodá-la e sem conhecer detalhes sobre os pacotes de gráficos. Sem rodar a função, responda: quantos gráficos a função plota? Qual o conteúdo de cada gráfico?

        Dois gráficos, um com a quantidade de lobos e coelhos, o outro com energia total e comida

    10) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu na parte 3?

        A quantidade de coelhos diminui a zero rapidamente e não se recupera, a comida fica baixo mas não nulo, já a quantidade de lobos e a quantidade de energia oscilam a longo prazo (observável a partir da casa dos 500 iterações) por causa da possibilidade de lobos comerem a cenoura

    11) Usando a combinação de constantes da questão 6 - que leva a ilha a ser dominada por coelhos - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        Não nascem lobos, a quantidade de coelhos, comida e energia são relativamente estáveis, resultado bem parecido com aquela analisada anteriormente

    12) Usando a combinação de constantes da questão 7 - que leva a ilha à extinção de todos os animais - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        Só tem cenouras e energia de cenouras. Resultado totalmente condizente com o esperado